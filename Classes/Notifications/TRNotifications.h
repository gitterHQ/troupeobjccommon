//
//  TRNotifications.h
//  Troupe
//
//  Created by Andrew Newdigate on 01/04/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const TroupeUrlChanged;
FOUNDATION_EXPORT NSString *const TroupeNotificationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeAuthenticationReady;
FOUNDATION_EXPORT NSString *const TroupeSignInSuccessful;
FOUNDATION_EXPORT NSString *const TroupeSignedOut;
FOUNDATION_EXPORT NSString *const TroupeSignOutRequested;
FOUNDATION_EXPORT NSString *const TroupeContextDidChange;
FOUNDATION_EXPORT NSString *const TroupeContextChanged;
FOUNDATION_EXPORT NSString *const IncomingUserNotification;
FOUNDATION_EXPORT NSString *const IncomingTroupeNotification;
FOUNDATION_EXPORT NSString *const UserIdChanged;
FOUNDATION_EXPORT NSString *const TroupeRealtimeActivated;
FOUNDATION_EXPORT NSString *const TroupeTitleChange;

FOUNDATION_EXPORT NSString *const TroupeUserUnreadCountsChanged;
FOUNDATION_EXPORT NSString *const TroupeUserMentionCountsChanged;

FOUNDATION_EXPORT NSString *const TroupeListChanged;
FOUNDATION_EXPORT NSString *const TroupeListError;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;
FOUNDATION_EXPORT NSString *const TroupeLocationAccessGranted;


FOUNDATION_EXPORT NSString *const TroupeRequireOAuth;
FOUNDATION_EXPORT NSString *const TroupeCompletedOAuth;

FOUNDATION_EXPORT NSString *const AuthTokenRevoked;

