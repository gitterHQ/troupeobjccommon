//
//  TRNotificationsManager.h
//  Troupe
//
//  Created by Andrew Newdigate on 21/11/2012.
//
//

#import <Foundation/Foundation.h>
#import "GTMOAuth2SignIn.h"

@interface TRNotificationsManager : NSObject

+ (TRNotificationsManager *) sharedInstance;

- (void) registerDeviceForNotifications:(NSData*) data forUniqueDeviceId:(NSString *) deviceId;
- (void) registerDeviceToUser:(NSString *) deviceId forAuthorization:(GTMOAuth2Authentication *) auth;

- (void) start;

@end
