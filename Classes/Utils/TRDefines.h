//
//  TRDefines.h
//  TroupeNotifier
//
//  Created by Andrew Newdigate on 07/04/2014.
//  Copyright (c) 2014 Andrew Newdigate. All rights reserved.
//

#ifndef TroupeNotifier_TRDefines_h
#define TroupeNotifier_TRDefines_h

#define PING_CHANNEL @"/api/v1/ping"
#define USER_CHANNEL @"/api/v1/user/"
#define TROUPES_SUBCHANNEL @"/rooms"


#endif
