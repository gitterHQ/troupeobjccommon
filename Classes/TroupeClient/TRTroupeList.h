//
//  TRTroupeList.h
//  Troupe
//
//  Created by Andrew Newdigate on 01/04/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TREventController.h"
#import "TRLiveCollection.h"
#import "TRTroupeModel.h"

@interface TRTroupeList : NSObject <TREventControllerDelegate, TRLiveCollectionChangeDelegate>

+ (TRTroupeList *) sharedInstance;

@property (nonatomic, readonly) NSInteger unreadTroupeCount;
@property (nonatomic, readonly) NSInteger mentionedTroupeCount;
@property (nonatomic, readonly) NSArray *favouriteTroupes;
@property (nonatomic, readonly) NSArray *unreadTroupes;
@property (nonatomic, readonly) NSArray *recentTroupes;
@property (nonatomic, readonly) NSArray *allConversations;

- (NSArray *)oneToOneTroupes;
- (NSArray *)orgRooms;
- (NSArray *)repoRooms;

- (void) startListening;
- (void) stopListening;

#if TARGET_OS_IPHONE
- (void) forceRefresh;
#endif

- (BOOL)connectionEstablished;


- (NSArray *) filter:(NSString *) searchText;
- (TRTroupeModel *) lastAccessedTroupe;
// - (void)resort;
@end
