//
//  TRAppSettings.h
//  Troupe
//
//  Created by Andrew Newdigate on 20/11/2012.
//
//

#import <Foundation/Foundation.h>


@interface TRAppSettings : NSObject


+ (TRAppSettings *) sharedInstance;

- (NSURL *) baseServerURL;

- (NSString *) baseServerURLString;

- (NSURL *) baseAPIURL;

- (NSURL *) locationPostURL;

- (NSURL *) oauthTokenURL;

- (NSString *) oauthRedirectURL;

- (NSURL *) oauthAuthorizationURL;

- (NSString*) keychainItemName;

- (NSString *) clientID;

- (NSString *) clientSecret;

- (NSString *) oauthScope;

#if TARGET_OS_IPHONE
- (NSString *) deviceName;

- (NSString *) uniqueDeviceId;
#endif

- (NSURL *) fromRelativeUrl:(NSString *) relativePath;

- (NSURL *) webSocketURL;

#if TARGET_OS_IPHONE

- (BOOL) reauthenticate;

- (BOOL) debugging;
- (BOOL) debuggingChanged;

- (BOOL) locationAccessRequested;
- (BOOL) notificationAccessRequested;

- (BOOL) locationAccessGranted;
- (BOOL) notificationAccessGranted;

- (void) setLocationAccessRequested;
- (void) setLocationAccessGranted;

- (NSString *)appVersion;

- (NSString *)appName;

- (void) setNotificationAccessRequested;
- (void) setNotificationAccessGranted;

- (BOOL)firstRunPostUpdate;

#endif


@end
